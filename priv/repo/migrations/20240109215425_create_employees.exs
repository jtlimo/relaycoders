defmodule Exercise.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees) do
      add :name, :string
      add :job_title, :string
      add :salary, :integer
      add :country_id, references(:countries, on_delete: :nilify_all)
      add :currency_id, references(:currencies, on_delete: :nilify_all)

      timestamps()
    end

    create index(:employees, [:country_id, :currency_id])
  end
end
