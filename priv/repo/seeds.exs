# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Exercise.Repo.insert!(%Exercise.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Exercise.Countries

# Seed the 8 supported currencies
# Euro (EUR)
# UK Pound Sterling (GBP)
# Australian Dollar (AUD)
# New Zealand Dollar (NZD)
# Unites States Dollar (USD)
# Canadian Dollar (CAD)
# Swiss Franc (CHF)
# Japanese Yen (JPY)
currency_data = [
  ["European Euro", "EUR", "€"],
  ["United Kingdom Pound Sterling", "GBP", "£"],
  ["Australian Dollar", "AUD", "$"],
  ["New Zealand Dollar", "NZD", "$"],
  ["United States Dollar", "USD", "$"],
  ["Canadian Dollar", "CAD", "$"],
  ["Swiss Franc", "CHF", "¥"],
  ["Japanese Yen", "JPY", "CHF"]
]

for currency <- currency_data do
  [name, code, symbol] = currency

  Countries.create_currency(%{
    name: name,
    code: code,
    symbol: symbol
  })
end

# Seed the 12 supported countries
country_data = [
  ["Australia", "AUS", "AUD"],
  ["Canada", "CAN", "CAD"],
  ["France", "FRA", "EUR"],
  ["Japan", "JPN", "JPY"],
  ["Italy", "ITA", "EUR"],
  ["Liechtenstein", "LIE", "CHF"],
  ["New Zealand", "NZL", "NZD"],
  ["Portugal", "PRT", "EUR"],
  ["Spain", "ESP", "EUR"],
  ["Switzerland", "CHE", "CHF"],
  ["United Kingdom", "GBR", "GBP"],
  ["United States", "USA", "USD"]
]

for country <- country_data do
  [name, code, currency_code] = country
  currency = Countries.get_currency_by_code!(currency_code)

  Countries.create_country(%{
    name: name,
    code: code,
    currency_id: currency.id
  })
end

alias Exercise.Employees

defmodule EmployeeCreator do
  @moduledoc """
    Creates 10000 employees to seed them in database
  """
  alias Exercise.Countries.Country

  @max_employees 10000

  @doc """
      seed 10000 employees in database.

  ## Examples

      iex> create_employees(first_names, last_names, job_titles)

  """
  def create_employees(first_names, last_names, job_titles) do
    first_names = cycle_names(first_names, @max_employees)
    last_names = cycle_names(last_names, @max_employees)
    job_titles = cycle_names(job_titles, @max_employees)

    create_employees_recursive(
      first_names,
      last_names,
      job_titles,
      cycle_names(Countries.list_countries(), @max_employees),
      0
    )
  end

  defp cycle_names(names, count) do
    Enum.take(repeat_names(names), count)
  end

  defp repeat_names(names) do
    Stream.cycle(names) |> Enum.take(@max_employees)
  end

  defp create_randomize_salary(min_salary \\ 100, max_salary \\ 999_999) do
    salary_in_dollars = :rand.uniform(max_salary - min_salary) + min_salary

    round(salary_in_dollars * 100)
  end

  defp create_employees_recursive(_, _, _, _, count) when count >= @max_employees, do: :ok

  defp create_employees_recursive([], _, _, _, count), do: :ok

  defp create_employees_recursive(_, [], _, _, count), do: :ok

  defp create_employees_recursive(_, _, [], _, count), do: :ok

  defp create_employees_recursive(_, _, _, [], count), do: :ok

  defp create_employees_recursive(
         [first_name | rest_first_names],
         [last_name | rest_last_names],
         [job_title | rest_job_titles],
         [country | rest_countries],
         count
       )
       when count < @max_employees do
    employee_name = "#{first_name} #{last_name}"

    %Country{id: id, currency_id: currency_id} = country

    Employees.create_employee(%{
      name: employee_name,
      job_title: job_title,
      salary: create_randomize_salary(),
      country_id: id,
      currency_id: currency_id
    })

    create_employees_recursive(
      rest_first_names,
      rest_last_names,
      rest_job_titles,
      rest_countries,
      count + 1
    )
  end
end

job_titles =
  "priv/data/job_titles.txt"
  |> File.read!()
  |> String.split(~r/\r?\n/, trim: true)
  |> Enum.filter(&(&1 != ""))

last_names =
  "priv/data/last_names.txt"
  |> File.read!()
  |> String.split(~r/\r?\n/, trim: true)
  |> Enum.filter(&(&1 != ""))

first_names =
  "priv/data/first_names.txt"
  |> File.read!()
  |> String.split(~r/\r?\n/, trim: true)
  |> Enum.filter(&(&1 != ""))

EmployeeCreator.create_employees(first_names, last_names, job_titles)
