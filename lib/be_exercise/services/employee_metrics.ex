defmodule Exercise.Services.EmployeeMetrics do
  @moduledoc """
  Module implementing the get salary metrics given a country and the type of metric
  """
  alias Exercise.Employees
  @allowed_types ["min", "avg", "max"]

  def salary_metrics_by_country(%{id: id}, type) do
    case type do
      t when t in @allowed_types ->
        Employees.get_salary_metric_from_employee_by_country!(id, t)

      _ ->
        {:error, %{message: "Type not allowed", allowed_types: @allowed_types}}
    end
  end

  def salary_metrics_by_job_title(job_title) do
    case Employees.get_salary_metric_from_employee_by_job_title!(job_title) do
      {:error, _reason} ->
        {:error,
         %{message: "Not found employees with this job title to calculate average salaries"}}

      salary ->
        salary
    end
  end
end
