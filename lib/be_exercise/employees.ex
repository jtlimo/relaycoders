defmodule Exercise.Employees do
  @moduledoc """
  The Employees context.
  """

  import Ecto.Query, warn: false
  alias Exercise.Repo

  alias Exercise.Employees.Employee

  @doc """
  Returns the list of employees.

  ## Examples

      iex> list_employees()
      [%Employee{}, ...]

  """
  def list_employees do
    Repo.all(Employee)
  end

  @doc """
  Gets a single employee.

  Returns nil if the Employee does not exist.

  ## Examples

      iex> get_employee!(123)
      {:ok, %Employee{}}

      iex> get_employee!(456)
      ** nil

  """
  def get_employee!(id), do: Repo.get(Employee, id)

  @doc """
  Gets employees by job title

  Returns nil if the job title does not exist.

  ## Examples

      iex> get_employee_by_job_title!("Engineer")
      %Employee{}

      iex> get_employee_by_job_title!("Teacher")
      ** nil

  """
  def get_employee_by_job_title!(job_title) do
    query =
      from(e in Employee,
        where: e.job_title == ^job_title
      )

    Repo.all(query)
  end

  @doc """
  Gets min / max or avg salary from an employee

  Returns minimum , maximum or average salary from employees given a country and the type of metric.

  ## Examples

      iex> get_salary_metric_from_employee_by_country!(1, "min")
      100

  """
  def get_salary_metric_from_employee_by_country!(country_id, type) do
    sql = """
    SELECT #{type}(salary)
    FROM employees
    WHERE country_id = $1
    """

    {:ok, result} = Repo.query(sql, [country_id])

    salary = List.first(result.rows) |> List.first()

    Decimal.round(salary, 0) |> Decimal.to_integer()
  end

  @doc """
  Gets average salary from employees

  Returns average salary from employees given a job title

  ## Examples

      iex> get_salary_metric_from_employee_by_job_title!("Scrum Master")
      100

  """
  def get_salary_metric_from_employee_by_job_title!(job_title) when is_nil(job_title),
    do: {:error, "Not possible to find job title with null value"}

  def get_salary_metric_from_employee_by_job_title!(job_title) do
    query =
      from(e in Employee,
        select: avg(e.salary),
        where: e.job_title == ^job_title
      )

    average_salary = Repo.one(query)

    case average_salary do
      nil ->
        {:error, "Job title not found"}

      _ ->
        Decimal.round(average_salary, 0) |> Decimal.to_integer()
    end
  end

  @doc """
  Creates an employee.

  ## Examples

      iex> create_employee(%{field: value})
       {:ok, %Employee{}}

      iex> create_employee(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_employee(attrs \\ %{}) do
    %Employee{}
    |> Employee.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a employee.

  ## Examples

      iex> update_employee(employee, %{field: new_value})
      {:ok, %Employee{}}

      iex> update_employee(employee, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_employee(%Employee{} = employee, attrs) do
    employee
    |> Employee.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes an employee.

  ## Examples

      iex> delete_employee(employee)
      {:ok, %Employee{}}

      iex> delete_employee(employee)
      {:error, %Ecto.Changeset{}}

  """
  def delete_employee(%Employee{} = employee) do
    Repo.delete(employee)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking employee changes.

  ## Examples

      iex> change_employee(employee)
      %Ecto.Changeset{data: %Employee{}}

  """
  def change_employee(%Employee{} = employee, attrs \\ %{}) do
    Employee.changeset(employee, attrs)
  end
end
