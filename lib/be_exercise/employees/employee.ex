defmodule Exercise.Employees.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  schema "employees" do
    field :name, :string
    field :job_title, :string
    field :salary, :integer
    field :currency_id, :id
    field :country_id, :id

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:name, :job_title, :salary, :country_id, :currency_id])
    |> validate_required([:name, :job_title, :salary])
    |> foreign_key_constraint(:country_id)
    |> foreign_key_constraint(:currency_id)
  end
end
