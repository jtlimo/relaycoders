defmodule ExerciseWeb.CountryController do
  alias Exercise.Services.EmployeeMetrics
  alias Exercise.Countries.Currency
  alias Exercise.Repo
  use ExerciseWeb, :controller

  alias Exercise.Countries
  alias Exercise.Countries.Country

  action_fallback ExerciseWeb.FallbackController

  def index(conn, _params) do
    countries = Countries.list_countries()
    render(conn, "index.json", countries: countries)
  end

  def create(conn, %{"country" => country_params}) do
    %{"currency_id" => currency_id} = country_params

    with nil <- Repo.get(Currency, currency_id) do
      conn
      |> put_status(:not_found)
      |> render("error.json", message: "Currency not found")
    end

    with {:ok, %Country{} = country} <- Countries.create_country(country_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.country_path(conn, :show, country))
      |> render("show.json", country: country)
    end
  end

  def show(conn, %{"id" => id}) do
    case Countries.get_country!(id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: "Country not found")

      country ->
        render(conn, "show.json", country: country)
    end
  end

  def update(conn, %{"id" => id, "country" => country_params}) do
    case Countries.get_country!(id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: "Country not found")

      country ->
        with {:ok, %Country{} = country} <- Countries.update_country(country, country_params) do
          render(conn, "show.json", country: country)
        end
    end
  end

  def delete(conn, %{"id" => id}) do
    case Countries.get_country!(id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: "Country not found")

      country ->
        with {:ok, %Country{}} <- Countries.delete_country(country) do
          send_resp(conn, :no_content, "")
        end
    end
  end

  def salary_metrics(conn, _params) do
    name = conn.query_params["name"]
    type = conn.query_params["type"]

    case Countries.get_country_by_name!(name) do
      nil ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: "Country not found")

      country ->
        case EmployeeMetrics.salary_metrics_by_country(country, type) do
          {:error, reason} ->
            conn
            |> put_status(:bad_request)
            |> render("error.json", message: reason)

          salary ->
            conn
            |> render("salary_metrics.json", salary: salary)
        end
    end
  end
end
