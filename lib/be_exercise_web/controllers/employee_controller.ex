defmodule ExerciseWeb.EmployeeController do
  use ExerciseWeb, :controller
  alias Exercise.Services.EmployeeMetrics
  alias Exercise.Employees
  alias Exercise.Employees.Employee
  alias Exercise.Countries.Country
  alias Exercise.Repo

  action_fallback ExerciseWeb.FallbackController

  def index(conn, _params) do
    employees = Employees.list_employees()
    render(conn, "index.json", employees: employees)
  end

  def show(conn, %{"id" => id}) do
    case Employees.get_employee!(id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: "Employee not found")

      employee ->
        render(conn, "show.json", employee: employee)
    end
  end

  def create(conn, %{"employee" => employee_params}) do
    %{"currency_id" => currency_id, "country_id" => country_id} = employee_params

    with nil <- Repo.get(Country, country_id) do
      conn
      |> put_status(:not_found)
      |> render("error.json", message: "Country not found")
    end

    with nil <- Repo.get(Country, currency_id) do
      conn
      |> put_status(:not_found)
      |> render("error.json", message: "Currency not found")
    end

    with {:ok, %Employee{} = employee} <- Employees.create_employee(employee_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.employee_path(conn, :show, employee))
      |> render("show.json", employee: employee)
    end
  end

  def update(conn, %{"id" => id, "employee" => employee_params}) do
    case Employees.get_employee!(id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: "Employee not found")

      employee ->
        with {:ok, %Employee{} = employee} <- Employees.update_employee(employee, employee_params) do
          render(conn, "show.json", employee: employee)
        end
    end
  end

  def delete(conn, %{"id" => id}) do
    case Employees.get_employee!(id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: "Employee not found")

      employee ->
        with {:ok, %Employee{}} <- Employees.delete_employee(employee) do
          send_resp(conn, :no_content, "")
        end
    end
  end

  def salary_metrics(conn, _params) do
    job_title = conn.query_params["job-title"]

    case EmployeeMetrics.salary_metrics_by_job_title(job_title) do
      {:error, reason} ->
        conn
        |> put_status(:not_found)
        |> render("error.json", message: reason)

      av ->
        conn
        |> put_status(:ok)
        |> render("salary_metrics.json", salary: av)
    end
  end
end
