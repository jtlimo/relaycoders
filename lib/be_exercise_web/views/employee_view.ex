defmodule ExerciseWeb.EmployeeView do
  use ExerciseWeb, :view
  alias ExerciseWeb.EmployeeView

  def render("index.json", %{employees: employees}) do
    %{data: render_many(employees, EmployeeView, "employee.json")}
  end

  def render("show.json", %{employee: employee}) do
    %{data: render_one(employee, EmployeeView, "employee.json")}
  end

  def render("error.json", %{message: message}) do
    %{errors: message}
  end

  def render("employee.json", %{employee: employee}) do
    %{
      id: employee.id,
      name: employee.name,
      job_title: employee.job_title,
      salary: employee.salary,
      country_id: employee.country_id,
      currency_id: employee.currency_id
    }
  end

  def render("salary_metrics.json", %{salary: salary}) do
    %{salary: salary}
  end
end
