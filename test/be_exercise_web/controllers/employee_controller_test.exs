defmodule ExerciseWeb.EmployeeControllerTest do
  alias Exercise.Countries.Country
  use ExerciseWeb.ConnCase

  alias Exercise.Countries
  alias Exercise.Employees
  alias Exercise.Employees.Employee
  alias Exercise.Countries.Currency

  @salary round(20 * 100)
  @minimum_wage round(2 * 100)
  @maximum_wage round(200 * 100)

  @create_attrs %{
    name: "Ruth Valery",
    job_title: "Scrum Master",
    salary: @salary
  }

  @create_employee_minimum_wage_attrs %{
    name: "Alex Smith",
    job_title: "Scrum Master",
    salary: @minimum_wage
  }

  @create_employee_maximum_wage_attrs %{
    name: "Rosalía Vila Tobella",
    job_title: "Scrum Master",
    salary: @maximum_wage
  }

  @update_attrs %{
    name: "Amy Adams",
    job_title: "Software Engineer",
    salary: @salary
  }

  @create_currency_attrs %{
    code: "some currency code",
    name: "some currency name",
    symbol: "some currency symbol"
  }

  @create_country_attrs %{
    code: "some country code",
    name: "some country name"
  }

  @invalid_attrs %{
    name: nil,
    job_title: nil,
    salary: nil
  }

  def fixture(:employee_maximum_wage) do
    country =
      Countries.get_country_by_name!(@create_country_attrs.name)

    %{id: country_id, currency_id: currency_id} = country

    updated_map =
      Map.put(
        Map.put(@create_employee_maximum_wage_attrs, :country_id, country_id),
        :currency_id,
        currency_id
      )

    {:ok, employee} = Employees.create_employee(updated_map)

    employee
  end

  def fixture(:employee_minimum_wage) do
    country =
      Countries.get_country_by_name!(@create_country_attrs.name)

    %{id: country_id, currency_id: currency_id} = country

    updated_map =
      Map.put(
        Map.put(@create_employee_minimum_wage_attrs, :country_id, country_id),
        :currency_id,
        currency_id
      )

    {:ok, employee} = Employees.create_employee(updated_map)

    employee
  end

  def fixture(:employee) do
    {:ok, %Currency{id: currency_id}} = Countries.create_currency(@create_currency_attrs)

    {:ok, country} =
      Countries.create_country(Map.put(@create_country_attrs, :currency_id, currency_id))

    %{id: country_id, currency_id: currency_id} = country

    updated_map =
      Map.put(Map.put(@create_attrs, :country_id, country_id), :currency_id, currency_id)

    {:ok, employee} = Employees.create_employee(updated_map)

    employee
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all employees", %{conn: conn} do
      conn = get(conn, Routes.employee_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "show" do
    setup [:create_employee]

    test "get successfully an employee", %{
      conn: conn,
      employee: %Employee{id: id, currency_id: currency_id, country_id: country_id}
    } do
      conn = get(conn, Routes.employee_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "name" => "Ruth Valery",
               "job_title" => "Scrum Master",
               "salary" => @salary,
               "country_id" => ^country_id,
               "currency_id" => ^currency_id
             } = json_response(conn, 200)["data"]
    end

    test "raises not found error when try to get a non existent employee", %{
      conn: conn,
      employee: %Employee{id: id}
    } do
      conn = get(conn, Routes.employee_path(conn, :show, id + 1))
      assert json_response(conn, 404)["errors"] == "Employee not found"
    end
  end

  describe "create an employee" do
    test "creates successfully an employee", %{conn: conn} do
      {:ok, %Currency{id: currency_id}} = Countries.create_currency(@create_currency_attrs)

      {:ok, country} =
        Countries.create_country(Map.put(@create_country_attrs, :currency_id, currency_id))

      %Country{id: country_id, currency_id: currency_id} = country

      updated_map =
        Map.put(Map.put(@create_attrs, :country_id, country_id), :currency_id, currency_id)

      conn = post(conn, Routes.employee_path(conn, :create), employee: updated_map)

      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.employee_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "name" => "Ruth Valery",
               "job_title" => "Scrum Master",
               "salary" => @salary,
               "country_id" => ^country_id,
               "currency_id" => ^currency_id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      {:ok, %Currency{id: currency_id}} = Countries.create_currency(@create_currency_attrs)

      {:ok, country} =
        Countries.create_country(Map.put(@create_country_attrs, :currency_id, currency_id))

      %Country{id: country_id, currency_id: currency_id} = country

      updated_map =
        Map.put(Map.put(@invalid_attrs, :country_id, country_id), :currency_id, currency_id)

      conn = post(conn, Routes.employee_path(conn, :create), employee: updated_map)

      assert json_response(conn, 422)["errors"] == %{
               "job_title" => ["can't be blank"],
               "name" => ["can't be blank"],
               "salary" => ["can't be blank"]
             }
    end
  end

  describe "update employee" do
    setup [:create_employee]

    test "renders employee when data is valid", %{
      conn: conn,
      employee: %Employee{id: id} = employee
    } do
      conn = put(conn, Routes.employee_path(conn, :update, employee), employee: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.employee_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "job_title" => "Software Engineer",
               "name" => "Amy Adams",
               "salary" => @salary
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, employee: employee} do
      conn = put(conn, Routes.employee_path(conn, :update, employee), employee: @invalid_attrs)

      assert json_response(conn, 422)["errors"] == %{
               "job_title" => ["can't be blank"],
               "name" => ["can't be blank"],
               "salary" => ["can't be blank"]
             }
    end
  end

  describe "delete employee" do
    setup [:create_employee]

    test "deletes chosen employee", %{conn: conn, employee: employee} do
      conn = delete(conn, Routes.employee_path(conn, :delete, employee))
      assert response(conn, 204)

      conn = get(conn, Routes.employee_path(conn, :show, employee))
      assert json_response(conn, 404)["errors"] == "Employee not found"
    end

    test "tries to delete a employee twice and receives an error ", %{
      conn: conn,
      employee: employee
    } do
      conn = delete(conn, Routes.employee_path(conn, :delete, employee))
      assert response(conn, 204)

      conn = delete(conn, Routes.employee_path(conn, :delete, employee))
      assert json_response(conn, 404)["errors"] == "Employee not found"
    end
  end

  describe "average salary metrics" do
    setup [:create_employee]
    setup [:create_employee_minimum_wage]
    setup [:create_employee_maximum_wage]

    test "given a job title returns the average employees salaries", %{
      conn: conn,
      employee: employee
    } do
      conn =
        get(
          conn,
          Routes.employee_path(conn, :salary_metrics, %{"job-title" => "Scrum Master"})
        )

      expected_response = %{"salary" => 7400}

      assert json_response(conn, 200) == expected_response
    end

    test "returns an error when tries to calculate average salary with a inexistent job title", %{
      conn: conn,
      employee: employee
    } do
      conn =
        get(
          conn,
          Routes.employee_path(conn, :salary_metrics, %{"job-title" => "Chef"})
        )

      assert json_response(conn, 404) == %{
               "errors" => %{
                 "message" =>
                   "Not found employees with this job title to calculate average salaries"
               }
             }
    end

    test "returns an error when tries to calculate average salary without a job title", %{
      conn: conn,
      employee: employee
    } do
      conn =
        get(
          conn,
          Routes.employee_path(conn, :salary_metrics)
        )

      assert json_response(conn, 404) == %{
               "errors" => %{
                 "message" =>
                   "Not found employees with this job title to calculate average salaries"
               }
             }
    end

    test "returns an error when tries to calculate average salaries with a job title that does not has any employee",
         %{
           conn: conn,
           employee: employee
         } do
      conn =
        get(
          conn,
          Routes.employee_path(conn, :salary_metrics, %{"job-title" => "Musician"})
        )

      assert json_response(conn, 404) == %{
               "errors" => %{
                 "message" =>
                   "Not found employees with this job title to calculate average salaries"
               }
             }
    end

    test "returns an error when tries to calculate average salaries with a nil job title ",
         %{
           conn: conn,
           employee: employee
         } do
      conn =
        get(
          conn,
          Routes.employee_path(conn, :salary_metrics, %{"job-title" => nil})
        )

      assert json_response(conn, 404) == %{
               "errors" => %{
                 "message" =>
                   "Not found employees with this job title to calculate average salaries"
               }
             }
    end
  end

  defp create_employee(_) do
    employee = fixture(:employee)
    %{employee: employee}
  end

  defp create_employee_minimum_wage(_) do
    employee = fixture(:employee_minimum_wage)
    %{employee: employee}
  end

  defp create_employee_maximum_wage(_) do
    employee = fixture(:employee_maximum_wage)
    %{employee: employee}
  end
end
