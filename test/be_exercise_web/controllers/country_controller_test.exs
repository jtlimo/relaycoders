defmodule ExerciseWeb.CountryControllerTest do
  alias Exercise.Employees.Employee
  use ExerciseWeb.ConnCase

  alias Exercise.Employees
  alias Exercise.Countries
  alias Exercise.Countries.Country
  alias Exercise.Countries.Currency

  @salary round(20 * 100)
  @minimum_wage round(2 * 100)
  @maximum_wage round(200 * 100)

  @create_employee_attrs %{
    name: "Ruth Valery",
    job_title: "Scrum Master",
    salary: @salary
  }

  @create_employee_minimum_wage_attrs %{
    name: "Alex Smith",
    job_title: "Chef",
    salary: @minimum_wage
  }

  @create_employee_maximum_wage_attrs %{
    name: "Rosalía Vila Tobella",
    job_title: "Musician",
    salary: @maximum_wage
  }

  @create_currency_attrs %{
    code: "some currency code",
    name: "some currency name",
    symbol: "some currency symbol"
  }

  @create_attrs %{
    code: "some code",
    name: "some name"
  }

  @currency_attrs %{
    code: "some code",
    name: "some name",
    symbol: "some symbol"
  }

  @update_attrs %{
    code: "some updated code",
    name: "some updated name"
  }
  @invalid_attrs %{code: nil, name: nil}

  def fixture(:employee_maximum_wage) do
    country =
      Countries.get_country_by_name!(@create_attrs.name)

    %{id: country_id, currency_id: currency_id} = country

    updated_map =
      Map.put(
        Map.put(@create_employee_maximum_wage_attrs, :country_id, country_id),
        :currency_id,
        currency_id
      )

    {:ok, employee} = Employees.create_employee(updated_map)

    employee
  end

  def fixture(:employee_minimum_wage) do
    country =
      Countries.get_country_by_name!(@create_attrs.name)

    %{id: country_id, currency_id: currency_id} = country

    updated_map =
      Map.put(
        Map.put(@create_employee_minimum_wage_attrs, :country_id, country_id),
        :currency_id,
        currency_id
      )

    {:ok, employee} = Employees.create_employee(updated_map)

    employee
  end

  def fixture(:employee) do
    {:ok, %Currency{id: currency_id}} = Countries.create_currency(@create_currency_attrs)

    {:ok, country} =
      Countries.create_country(Map.put(@create_attrs, :currency_id, currency_id))

    %{id: country_id, currency_id: currency_id} = country

    updated_map =
      Map.put(Map.put(@create_employee_attrs, :country_id, country_id), :currency_id, currency_id)

    {:ok, employee} = Employees.create_employee(updated_map)

    employee
  end

  def fixture(:country) do
    {:ok, %Currency{id: currency_id}} = Countries.create_currency(@currency_attrs)

    {:ok, country} = Countries.create_country(Map.put(@create_attrs, :currency_id, currency_id))
    country
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all countries", %{conn: conn} do
      conn = get(conn, Routes.country_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create country" do
    test "create country with associated currency", %{conn: conn} do
      {:ok, %Currency{id: currency_id}} = Countries.create_currency(@currency_attrs)
      country_params = %{name: "some name", code: "some code", currency_id: currency_id}

      conn = post(conn, Routes.country_path(conn, :create), country: country_params)

      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.country_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "code" => "some code",
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      {:ok, %Currency{id: currency_id}} = Countries.create_currency(@currency_attrs)

      country_params = %{
        name: nil,
        code: nil,
        currency_id: currency_id
      }

      conn = post(conn, Routes.country_path(conn, :create), country: country_params)

      assert json_response(conn, 422)["errors"] == %{
               "code" => ["can't be blank"],
               "name" => ["can't be blank"]
             }
    end
  end

  describe "update country" do
    setup [:create_country]

    test "renders country when data is valid", %{conn: conn, country: %Country{id: id} = country} do
      conn = put(conn, Routes.country_path(conn, :update, country), country: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.country_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "code" => "some updated code",
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, country: country} do
      conn = put(conn, Routes.country_path(conn, :update, country), country: @invalid_attrs)

      assert json_response(conn, 422)["errors"] == %{
               "code" => ["can't be blank"],
               "name" => ["can't be blank"]
             }
    end
  end

  describe "delete country" do
    setup [:create_country]

    test "deletes chosen country", %{conn: conn, country: country} do
      conn = delete(conn, Routes.country_path(conn, :delete, country))
      assert response(conn, 204)

      conn = get(conn, Routes.country_path(conn, :show, country))
      assert json_response(conn, 404)["errors"] == "Country not found"
    end

    test "tries to delete a country twice and receives an error ", %{conn: conn, country: country} do
      conn = delete(conn, Routes.country_path(conn, :delete, country))
      assert response(conn, 204)

      conn = delete(conn, Routes.country_path(conn, :delete, country))
      assert json_response(conn, 404)["errors"] == "Country not found"
    end
  end

  describe "minimum salary metrics" do
    setup [:create_employee]
    setup [:create_employee_minimum_wage]

    test "returns country not found when tries to find a non existent country", %{conn: conn} do
      conn = get(conn, Routes.country_path(conn, :salary_metrics), %{"name" => "Bangladesh"})

      assert json_response(conn, 404)["errors"] == "Country not found"
    end

    test "returns bad request error when tries to search with an invalid type of metric", %{
      conn: conn
    } do
      conn =
        get(conn, Routes.country_path(conn, :salary_metrics), %{
          "name" => "some name",
          "type" => "invalid"
        })

      assert json_response(conn, 400)["errors"] == %{
               "message" => "Type not allowed",
               "allowed_types" => [
                 "min",
                 "avg",
                 "max"
               ]
             }
    end

    test "returns bad request error when tries to search without a metric", %{
      conn: conn
    } do
      conn =
        get(conn, Routes.country_path(conn, :salary_metrics), %{
          "name" => "some name"
        })

      assert json_response(conn, 400)["errors"] == %{
               "message" => "Type not allowed",
               "allowed_types" => [
                 "min",
                 "avg",
                 "max"
               ]
             }
    end

    test "returns bad request error when tries to search with an empty string metric", %{
      conn: conn
    } do
      conn =
        get(conn, Routes.country_path(conn, :salary_metrics), %{
          "name" => "some name",
          "type" => "  "
        })

      assert json_response(conn, 400)["errors"] == %{
               "message" => "Type not allowed",
               "allowed_types" => [
                 "min",
                 "avg",
                 "max"
               ]
             }
    end

    test "given a country returns the minimum employee salary", %{
      conn: conn,
      employee: employee
    } do
      conn =
        get(
          conn,
          Routes.country_path(conn, :salary_metrics, %{"name" => "some name", "type" => "min"})
        )

      expected_response = %{"salary" => 200}

      assert json_response(conn, 200) == expected_response
    end
  end

  describe "maximum salary metrics" do
    setup [:create_employee]
    setup [:create_employee_maximum_wage]

    test "given a country returns the maximum employee salary", %{
      conn: conn,
      employee: employee
    } do
      conn =
        get(
          conn,
          Routes.country_path(conn, :salary_metrics, %{"name" => "some name", "type" => "max"})
        )

      expected_response = %{"salary" => 20000}

      assert json_response(conn, 200) == expected_response
    end
  end

  describe "average salary metrics" do
    setup [:create_employee]
    setup [:create_employee_minimum_wage]
    setup [:create_employee_maximum_wage]

    test "given a country returns the average employee salary", %{
      conn: conn,
      employee: employee
    } do
      conn =
        get(
          conn,
          Routes.country_path(conn, :salary_metrics, %{"name" => "some name", "type" => "avg"})
        )

      expected_response = %{"salary" => 7400}

      assert json_response(conn, 200) == expected_response
    end
  end

  defp create_country(_) do
    country = fixture(:country)
    %{country: country}
  end

  defp create_employee(_) do
    employee = fixture(:employee)
    %{employee: employee}
  end

  defp create_employee_minimum_wage(_) do
    employee = fixture(:employee_minimum_wage)
    %{employee: employee}
  end

  defp create_employee_maximum_wage(_) do
    employee = fixture(:employee_maximum_wage)
    %{employee: employee}
  end
end
